/*
 * square_wave.rs
 * Defines a structure that represents a sound wave
 * Created on 9/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//crate import
extern crate sdl2;

//import
use sdl2::audio::AudioCallback;

/// A sound wave 
pub struct SquareWave {
    freq: f32,
    phase: f32,
    volume: f32
}

//implementation
impl SquareWave {
    /// Creates a new `SquareWave` instance
    ///
    /// # Arguments
    ///
    /// * `new_freq` - The frequency of the sound 
    /// * `new_phase` - The phase of the sound 
    /// * `new_volume` The volume of the sound
    /// 
    /// # Returns
    ///
    /// A new `SquareWave` instance with
    /// the given properties
    pub fn new(new_freq: f32, new_phase: f32, 
                new_volume: f32) -> SquareWave {
        return SquareWave {
            freq: new_freq,
            phase: new_phase,
            volume: new_volume 
        };
    }
}

//AudioCallback implementation
impl AudioCallback for SquareWave {
    type Channel = f32;

    /// Propagates the `SquareWave`
    ///
    /// # Argument
    ///
    /// * `out` - The sound data to modify 
    fn callback(&mut self, out: &mut [f32]) {
        // Generate a square wave
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 { self.volume } else { -self.volume };
            self.phase = (self.phase + self.freq) % 1.0;
        }
    }
}

//end of file
