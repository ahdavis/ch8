/*
 * beeper.rs
 * Defines a structure that plays a beeping sound
 * Created on 9/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//crate import
extern crate sdl2;

//imports
use super::SquareWave;
use sdl2::audio::AudioSpecDesired;
use sdl2::AudioSubsystem;
use sdl2::audio::AudioDevice;

/// Used to generate a beeping sound
pub struct Beeper {
    device: AudioDevice<SquareWave>, //the audio device
    is_playing: bool //is the sound currently playing?
}

//implementation
impl Beeper {
    /// Creates a new `Beeper` instance
    ///
    /// # Argument
    ///
    /// * `ctxt` - The SDL audio context
    ///
    /// # Returns
    ///
    /// A new `Beeper` instance
    ///
    /// # Panics
    ///
    /// This method will panic if the audio
    /// device fails to initialize. 
    pub fn new(ctxt: &mut AudioSubsystem) -> Beeper {
        //initialize the audio spec
        let desired_spec = AudioSpecDesired {
            freq: Some(44_100),
            channels: Some(1), //mono
            samples: None //use default sample size
        };

        //create the audio device
        let new_device = ctxt.open_playback(None, &desired_spec, |spec| {
            SquareWave::new(440.0 / spec.freq as f32, 0.0, 0.25)
        }).unwrap();

        //and return the beeper
        return Beeper {
            device: new_device,
            is_playing: false
        };
    }

    /// Gets whether the sound is playing
    /// 
    /// # Returns
    ///
    /// Whether the sound is playing
    pub fn is_playing(&self) -> bool {
        return self.is_playing;
    }

    /// Starts playing the sound
    pub fn play(&mut self) {
        self.device.resume();
        self.is_playing = true; 
    }

    /// Stops playing the sound
    pub fn stop(&mut self) {
        self.device.pause();
        self.is_playing = false;
    }
}

//Drop implementation
impl Drop for Beeper {
    /// Called when the instance
    /// goes out of scope
    fn drop(&mut self) {
        self.stop();
    }
}

//end of file
