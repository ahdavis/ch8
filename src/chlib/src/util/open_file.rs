/*
 * open_file.rs
 * Defines a function that allows the user to open a file
 * Created on 9/5/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//crate import
extern crate nfd;

//import
use nfd::Response;

/// Shows an open file dialog
///
/// # Returns
///
/// The path to the chosen file
/// 
/// # Panics
///
/// This function will panic if opening
/// the dialog fails
pub fn open_file() -> String {
    //define the filter list
    let filter = Some("c8");

    //show the dialog
    let result = nfd::open_file_dialog(filter, None).unwrap_or_else(|e| {
        panic!(e);
    });

    //and return the chosen file
    return match result {
        Response::Okay(file) => file,
        Response::OkayMultiple(files) => files[0].to_owned(),
        Response::Cancel => open_file()
    }
}

//end of file
