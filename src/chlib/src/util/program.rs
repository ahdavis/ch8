/*
 * program.rs
 * Defines a struct that represents a Chip-8 program
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use std::fs::File;
use std::io::Read;

/// Manages a Chip-8 binary
pub struct Program {
    bytes: Vec<u8>, //the bytes that make up the program
    length: usize,  //the number of bytes in the program
}

//implementation
impl Program {
    /// Creates a new `Program` instance
    ///
    /// # Argument
    ///
    /// * `path` - The path to the Chip-8 ROM to load
    ///
    /// # Returns
    ///
    /// A `Result` containing the new `Program` instance
    pub fn new(path: &str) -> Result<Program, std::io::Error> {
        //attempt to open the file
        let f = File::open(path);

        //check it for errors
        let mut f = match f {
            Ok(file) => {
                file 
            }
            Err(e) => {
                return Err(e);
            }
        };

        //create the read buffer
        let mut buf: Vec<u8> = Vec::new();

        //and read the bytes from the file
        return match f.read_to_end(&mut buf) {
            Ok(_) => {
                let len = buf.len();
                Ok(Program {
                    bytes: buf,
                    length: len
                })
            }
            Err(e) => {
                return Err(e)
            }
        };
    }

    /// Gets the bytes that make up the `Program`
    ///
    /// # Returns
    ///
    /// The bytes (`u8`) that make up the program
    pub fn get_bytes(&self) -> &Vec<u8> {
        return &self.bytes;
    }

    /// Gets the length (in bytes) of the `Program`
    ///
    /// # Returns
    ///
    /// The length (in bytes) of the program
    pub fn len(&self) -> usize {
        return self.length;
    }
}

//end of file
