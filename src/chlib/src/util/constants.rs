/*
 * constants.rs
 * Defines constants for ch8
 * Created on 8/30/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//constant definitions

/// The width of the game window
/// Measured in emulator pixels,
/// not screen pixels
pub const WIN_WIDTH: u32 = 64;

/// The height of the game window
/// Measured in emulator pixels,
/// not screen pixels
pub const WIN_HEIGHT: u32 = 32;

/// The size of an emulator pixel
/// Measured in screen pixels
pub const PIXEL_DIM: u32 = 16;

//end of file
