/*
 * keycode.rs
 * Enumerates input keycodes
 * Created on 9/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/// The identifier of a `Key`
#[derive(Debug, PartialEq, Eq, Copy, Hash)]
pub enum Keycode {
    K1 = 0x0, //numeric 1 key
    K2 = 0x1, //numeric 2 key
    K3 = 0x2, //numeric 3 key
    K4 = 0x3, //numeric 4 key
    KQ = 0x4, //Q key
    KW = 0x5, //W key
    KE = 0x6, //E key
    KR = 0x7, //R key
    KA = 0x8, //A key
    KS = 0x9, //S key
    KD = 0xA, //D key
    KF = 0xB, //F key
    KZ = 0xC, //Z key
    KX = 0xD, //X key
    KC = 0xE, //C key
    KV = 0xF  //V key
}

//implementation
impl Keycode {
    /// Creates a `Keycode` instance from
    /// an integer
    ///
    /// # Argument
    ///
    /// * `code` - The integer to generate the `Keycode` for
    /// 
    /// # Returns
    ///
    /// The `Keycode` associated with `code`
    ///
    /// # Panics
    ///
    /// This method will panic if `code` is more than 15 (0xF). 
    pub fn from_u32(code: u32) -> Keycode {
        return match code {
            0 => Keycode::K1,
            1 => Keycode::K2,
            2 => Keycode::K3,
            3 => Keycode::K4,
            4 => Keycode::KQ,
            5 => Keycode::KW,
            6 => Keycode::KE,
            7 => Keycode::KR,
            8 => Keycode::KA,
            9 => Keycode::KS,
            10 => Keycode::KD,
            11 => Keycode::KF,
            12 => Keycode::KZ,
            13 => Keycode::KX,
            14 => Keycode::KC,
            15 => Keycode::KV, 
            _ => panic!("Unknown keycode: {}", code)
        };
    }
}

// Clone implementation
impl Clone for Keycode {
    /// Clones the `Keycode`
    ///
    /// # Returns
    ///
    /// A clone of the enum instance
    fn clone(&self) -> Self {
        return *self;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import Keycode code
    use super::*;

    //this test checks keycode generation
    //from integers
    #[test]
    fn test_keycode_gen() {
        //create a list of keycodes
        let keycodes = [
            Keycode::K1,
            Keycode::K2,
            Keycode::K3,
            Keycode::K4,
            Keycode::KQ,
            Keycode::KW,
            Keycode::KE,
            Keycode::KR,
            Keycode::KA,
            Keycode::KS,
            Keycode::KD,
            Keycode::KF,
            Keycode::KZ,
            Keycode::KX,
            Keycode::KC,
            Keycode::KV 
        ];

        //and test keycode generation
        for i in 0..16 {
            assert_eq!(keycodes[i], Keycode::from_u32(i as u32));
        }
    }

    //this test checks invalid keycode generation
    #[test]
    #[should_panic]
    fn test_invalid_keycode_gen() {
        let _unused = Keycode::from_u32(16);
    }
}

//end of file
