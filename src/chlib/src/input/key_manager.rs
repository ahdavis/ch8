/*
 * key_manager.rs
 * Defines a struct that manages keys
 * Created on 9/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use super::Key;
use super::Keycode;
use super::KeyState;
use std::collections::HashMap;
use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::keyboard::Keycode as SDLKeycode;

/// Manages key input
pub struct KeyManager<'a> {
    keys: HashMap<Keycode, Key>, //the managed keys
    event_mgr: &'a mut EventPump //used to get key events
}

//implementation
impl<'a> KeyManager<'a> {
    /// Creates a new `KeyManager` instance
    ///
    /// # Argument
    ///
    /// * `new_mgr` - The SDL event manager to get keys from
    ///
    /// # Returns
    ///
    /// A new `KeyManager` instance with the given event manager
    pub fn new(new_mgr: &'a mut EventPump) -> KeyManager {
        //generate the hashmap for the keys
        let mut map: HashMap<Keycode, Key> = HashMap::new();
        for c in 0..16 {
            let code = Keycode::from_u32(c);
            map.insert(code, Key::new(code));
        }

        //and return the instance
        return KeyManager {
            keys: map,
            event_mgr: new_mgr 
        };
    }

    /// Returns whether a given `Key` is down
    ///
    /// # Argument
    ///
    /// * `code` - The `Keycode` to examine
    ///
    /// # Returns
    ///
    /// Whether the `Key` with the given code is down
    pub fn key_is_down(&self, code: Keycode) -> bool {
        return self.keys.get(&code).unwrap().state() == KeyState::DOWN;
    }

    /// Returns whether a given `Key` is up
    /// 
    /// # Argument
    ///
    /// * `code` - The `Keycode` to examine
    ///
    /// # Returns
    ///
    /// Whether the `Key` with the given code is up
    pub fn key_is_up(&self, code: Keycode) -> bool {
        return self.keys.get(&code).unwrap().state() == KeyState::UP;
    }

    /// Polls key events and updates key states
    ///
    /// # Returns
    ///
    /// Whether a quit event was polled
    pub fn poll_events(&mut self) -> bool {
        //loop and poll events
        let mut press_codes: Vec<SDLKeycode> = Vec::new();
        let mut release_codes: Vec<SDLKeycode> = Vec::new();
        for event in self.event_mgr.poll_iter() {
            match event {
                Event::KeyDown {keycode, ..} => {
                    match keycode {
                        Some(c) => {
                            press_codes.push(c)
                        },
                        None => {
                            //do nothing 
                        }
                    }
                },
                Event::KeyUp {keycode, ..} => {
                    match keycode {
                        Some(c) => {
                            release_codes.push(c);
                        },
                        None => {
                            //do nothing
                        }
                    }
                },
                Event::Quit {..} => {
                    return true;
                },
                _ => {
                    //do nothing
                }
            }
        }

        //process the press and release codes
        for c in press_codes {
            self.press_sdl(c);
        }
        for c in release_codes {
            self.release_sdl(c);
        }

        //if control reaches here, then a quit
        //event was not polled
        //so we return false
        return false;
    }

    /// Waits for a keypress and returns
    /// its `Keycode`
    ///
    /// # Returns
    ///
    /// The `Keycode` of the pressed key
    pub fn wait_key(&mut self) -> Keycode {
        //wait for an event
        let evt = self.event_mgr.wait_event();

        //and match it
        return match evt {
            Event::KeyDown {keycode, ..} => {
                match keycode {
                    Some(kc) => {
                        match self.code_for_sdl(kc) {
                            Some(c) => {
                                self.keys.get_mut(&c).unwrap().press();
                                c
                            }

                            None => {
                                self.wait_key()
                            }
                        }
                    }

                    None => {
                        self.wait_key()
                    }
                }
            },
            _ => {
                self.wait_key()
            }
        };
    }

    /// Returns the `Keycode` for a given
    /// SDL keycode
    /// 
    /// # Argument
    ///
    /// * `sdl_code` - The SDL keycode to convert
    ///
    /// # Returns
    ///
    /// The corresponding Chip-8 `Keycode`, wrapped
    /// in an `Option`
    fn code_for_sdl(&self, sdl_code: SDLKeycode) -> Option<Keycode> {
        //match the keycode
        return match sdl_code {
            SDLKeycode::Num1 => Some(Keycode::K1),
            SDLKeycode::Num2 => Some(Keycode::K2),
            SDLKeycode::Num3 => Some(Keycode::K3),
            SDLKeycode::Num4 => Some(Keycode::K4),
            SDLKeycode::Q => Some(Keycode::KQ),
            SDLKeycode::W => Some(Keycode::KW),
            SDLKeycode::E => Some(Keycode::KE),
            SDLKeycode::R => Some(Keycode::KR),
            SDLKeycode::A => Some(Keycode::KA),
            SDLKeycode::S => Some(Keycode::KS),
            SDLKeycode::D => Some(Keycode::KD),
            SDLKeycode::F => Some(Keycode::KF),
            SDLKeycode::Z => Some(Keycode::KZ),
            SDLKeycode::X => Some(Keycode::KX),
            SDLKeycode::C => Some(Keycode::KC),
            SDLKeycode::V => Some(Keycode::KV),
            _ => None
        };
    }

    /// Presses the `Key` associated with a
    /// given SDL keycode
    /// 
    /// # Argument
    ///
    /// * `sdl_code` - The SDL keycode to press
    fn press_sdl(&mut self, sdl_code: SDLKeycode) {
        match self.code_for_sdl(sdl_code) {
            Some(c) => {
                self.keys.get_mut(&c).unwrap().press();
            }
            None => {
                //do nothing 
            }
        };
    }

    /// Releases the `Key` associated with a 
    /// given SDL keycode
    /// 
    /// # Argument
    ///
    /// * `sdl_code` - The SDL keycode to release
    fn release_sdl(&mut self, sdl_code: SDLKeycode) {
        match self.code_for_sdl(sdl_code) {
            Some(c) => {
                self.keys.get_mut(&c).unwrap().release();
            }
            None => {
                //do nothing 
            }
        };
    }
}
