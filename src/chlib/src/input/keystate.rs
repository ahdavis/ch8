/*
 * keystate.rs
 * Enumerates states of input keys
 * Created on 9/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/// The state of an input `Key`
#[derive(Debug, PartialEq, Copy)]
pub enum KeyState {
    UP,   //key is not pressed
    DOWN  //key is pressed
}

//Not implementation
impl std::ops::Not for KeyState {
    //define the output type
    type Output = KeyState;

    /// Inverts the `KeyState`
    ///
    /// # Returns
    ///
    /// The opposite state from `self`
    fn not(self) -> Self::Output {
        return match self {
            KeyState::DOWN => {
                KeyState::UP 
            },
            KeyState::UP => {
                KeyState::DOWN 
            }
        };
    }
}

//Clone implementation
impl Clone for KeyState {
    /// Clones the state
    /// 
    /// # Returns
    ///
    /// A clone of the enum instance
    fn clone(&self) -> Self {
        return *self;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the KeyState code
    use super::*;

    //this test checks inversion of 
    //a KeyState
    #[test]
    fn test_inversion() {
        let s1 = KeyState::DOWN;
        let s2 = KeyState::UP;
        assert_eq!(!s1, KeyState::UP);
        assert_eq!(!s2, KeyState::DOWN);
    }
}

//end of file
