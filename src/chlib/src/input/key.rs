/*
 * key.rs
 * Defines a struct that represents a key
 * Created on 9/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use super::Keycode;
use super::KeyState;

/// An input key
pub struct Key {
   code: Keycode,   //the code associated with the key
   state: KeyState  //the state of the key
}

//implementation
impl Key {
    /// Creates a new `Key` instance
    ///
    /// # Argument
    ///
    /// * `new_code` - The keycode associated with the key
    /// 
    /// # Returns
    ///
    /// A new `Key` instance with the given code,
    /// with its state set to `KeyState::UP`. 
    pub fn new(new_code: Keycode) -> Key {
        return Key {
            code: new_code,
            state: KeyState::UP 
        };
    }

    /// Gets the code for the `Key`
    ///
    /// # Returns
    ///
    /// The `Keycode` associated with the `Key`
    pub fn code(&self) -> Keycode {
        return self.code;
    }

    /// Gets the state of the `Key`
    ///
    /// # Returns 
    ///
    /// The state of the `Key`
    pub fn state(&self) -> KeyState {
        return self.state;
    }

    /// Toggles the state of the `Key`
    ///
    /// # Returns
    ///
    /// The new state of the `Key`
    pub fn toggle(&mut self) -> KeyState {
        self.state = !self.state; //toggle the state
        return self.state; //and return the new state
    }

    /// Presses the `Key`
    pub fn press(&mut self) {
        self.state = KeyState::DOWN;
    }

    /// Releases the `Key`
    pub fn release(&mut self) {
        self.state = KeyState::UP;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the Key code
    use super::*;

    //this test checks that new keys are
    //always up
    #[test]
    fn test_keys_start_up() {
        let key = Key::new(Keycode::K1);
        assert_eq!(key.state, KeyState::UP);
    }

    //this test checks toggling of keys
    #[test]
    fn test_toggle() {
        let mut key = Key::new(Keycode::K1);
        assert_eq!(key.toggle(), KeyState::DOWN);
        assert_eq!(key.toggle(), KeyState::UP);
        assert_eq!(key.toggle(), KeyState::DOWN);
    }
}

//end of file
