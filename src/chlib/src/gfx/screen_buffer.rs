/*
 * screen_buffer.rs
 * Defines a struct that represents a screen buffer
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use super::Pixel;
use super::PixelState;
use sdl2::rect::Rect;
use super::super::util::constants;

/// Manages screen pixels
pub struct ScreenBuffer {
    data: [Pixel; (constants::WIN_WIDTH * constants::WIN_HEIGHT) as usize] //the pixels that make up the screen
}

//implementation
impl ScreenBuffer {
    /// Creates a new `ScreenBuffer` instance
    ///
    /// # Returns
    ///
    /// A new `ScreenBuffer` instance
    pub fn new() -> ScreenBuffer {
        //create the array field of the object
        let array = unsafe {
            //create the array as uninitialized memory
            let mut array: [Pixel; (constants::WIN_WIDTH * constants::WIN_HEIGHT) as usize] = std::mem::uninitialized();
            
            //populate it with pixels
            for (i, elem) in array.iter_mut().enumerate() {
                let ci = i as u32; //cast i to a u32
                let cur_pix = Pixel::new(ci % constants::WIN_WIDTH, ci / constants::WIN_WIDTH); //create the pixel
                std::ptr::write(elem, cur_pix); //and write the pixel to the array
            }

            //and return the array
            array 
        };

        //and generate the object
        return ScreenBuffer {
            data: array 
        };
    }

    /// Returns the state of the `Pixel` at
    /// given coordinates on the screen
    /// 
    /// # Arguments
    ///
    /// * `x` The x-coordinate of the pixel to check
    /// * `y` The y-coordinate of the pixel to check
    ///
    /// # Returns
    ///
    /// The state of the pixel at `(x, y)` on the screen
    pub fn state_at_coords(&self, x: u32, y: u32) -> PixelState {
        //validate the coordinates
        if (x >= constants::WIN_WIDTH) || (y >= constants::WIN_HEIGHT) {
            return PixelState::OFF;
        }

        //get the array offset from the coordinates
        let offset = (y * constants::WIN_WIDTH + x) as usize;

        //and return the pixel state at that offset
        return self.data[offset].get_state();
    }

    /// Toggles the `Pixel` at given coordinates
    /// on the screen
    /// 
    /// # Arguments
    ///
    /// * `x` The x-coordinate of the pixel to toggle
    /// * `y` The y-coordinate of the pixel to toggle
    ///
    /// # Returns
    ///
    /// Whether the pixel was on and then turned off 
    pub fn xor_at_coords(&mut self, x: u32, y: u32) -> bool {
        //validate the coordinates
        if (x >= constants::WIN_WIDTH) || (y >= constants::WIN_HEIGHT) {
            return false;
        }

        //get the array offset from the coordinates
        let offset = (y * constants::WIN_WIDTH + x) as usize;

        //get the state of that pixel
        let old_state = self.data[offset].get_state();

        //toggle the pixel
        self.data[offset].toggle();

        //and return whether the pixel was on and then turned off
        return old_state == PixelState::ON;
    }

    /// Returns the bounds of the `Pixel` at given coordinates
    ///
    /// # Arguments
    ///
    /// * `x` - The x-coordinate of the pixel to inspect
    /// * `y` - The y-coordinate of the pixel to inspect
    ///
    /// # Returns
    ///
    /// The bounds of the pixel at the given coordinates
    /// wrapped in an `Option`
    pub fn bounds_at_coords(&self, x: u32, y: u32) -> Option<Rect> {
        //validate the coordinates
        if (x >= constants::WIN_WIDTH) || (y >= constants::WIN_HEIGHT) {
            return None;
        }

        //get the array offset from the coordinates
        let offset = (y * constants::WIN_WIDTH + x) as usize;

        //and return the bounds of that pixel
        return Some(self.data[offset].get_bounds());
    }
    
    /// Clears the screen, turning all pixels off
    pub fn clear(&mut self) {
        //loop and turn all the pixels off
        for pix in self.data.iter_mut() {
            pix.turn_off();
        }
    }
}

//trait implementations

//Drop trait implementation
impl Drop for ScreenBuffer {
    /// Called when the `ScreenBuffer` goes out of scope
    fn drop(&mut self) {
        self.clear(); //clear the buffer
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the ScreenBuffer code
    use super::*;

    //this test checks modifying pixels
    //on the screen
    #[test]
    fn test_pixel_modify() {
        let mut screen = ScreenBuffer::new();
        assert_eq!(screen.state_at_coords(0, 0), PixelState::OFF);
        assert_eq!(screen.xor_at_coords(0, 0), false);
        assert_eq!(screen.state_at_coords(0, 0), PixelState::ON);
        assert_eq!(screen.xor_at_coords(0, 0), true);
        assert_eq!(screen.state_at_coords(0, 0), PixelState::OFF);
    }
    
    //this test checks out of bounds coordinates
    //being given to the state_at_coords method
    #[test]
    fn test_oob_state() {
        let screen = ScreenBuffer::new();
        assert_eq!(screen.state_at_coords(constants::WIN_WIDTH, constants::WIN_HEIGHT), PixelState::OFF);
    }

    //this test checks out of bounds coordinates
    //being given to the xor_at_coords method
    #[test]
    fn test_oob_xor() {
        let mut screen = ScreenBuffer::new();
        assert_eq!(screen.xor_at_coords(constants::WIN_WIDTH, constants::WIN_HEIGHT), false);
    }

    //this test checks clearing the screen
    #[test]
    fn test_clear() {
        let mut screen = ScreenBuffer::new();
        screen.xor_at_coords(2, 4);
        assert_eq!(screen.state_at_coords(2, 4), PixelState::ON);
        screen.clear();
        assert_eq!(screen.state_at_coords(2, 4), PixelState::OFF);
    }

    //this test checks getting bounds of pixels
    #[test]
    fn test_bounds() {
        let screen = ScreenBuffer::new();
        let bounds = screen.bounds_at_coords(1, 2);
        assert!(bounds.is_some());
        let bounds = bounds.unwrap();
        assert_eq!(bounds.x() as u32, 1 * constants::PIXEL_DIM);
        assert_eq!(bounds.y() as u32, 2 * constants::PIXEL_DIM);
        assert_eq!(bounds.width(), constants::PIXEL_DIM);
        assert_eq!(bounds.height(), constants::PIXEL_DIM);
        let bounds = screen.bounds_at_coords(constants::WIN_WIDTH, constants::WIN_HEIGHT);
        assert!(bounds.is_none());
    }
}

//end of file
