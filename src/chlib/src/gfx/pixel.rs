/*
 * pixel.rs
 * Defines a struct that represents a display pixel
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use super::PixelState;
use sdl2::rect;
use super::super::util::constants;

/// A single emulator pixel
pub struct Pixel {
    state: PixelState, //the state of the pixel
    x: u32, //the x-coord of the top left corner of the pixel
    y: u32  //the y-coord of the top left corner of the pixel 
}

//implementation
impl Pixel {
    /// Creates a new `Pixel` instance
    ///
    /// # Arguments
    ///
    /// * `new_x` The x-coord of the `Pixel`
    /// * `new_y` The y-coord of the `Pixel`
    ///
    /// # Returns
    ///
    /// A new `Pixel` with the given properties
    pub fn new(new_x: u32, new_y: u32) -> Pixel {
        return Pixel {
            state: PixelState::OFF,
            x: new_x,
            y: new_y 
        };
    }

    /// Gets the bounds of the `Pixel`
    ///
    /// # Returns
    ///
    /// The bounds of the `Pixel`
    pub fn get_bounds(&self) -> rect::Rect {
        //calculate the actual coordinates
        let cx = self.x * constants::PIXEL_DIM;
        let cy = self.y * constants::PIXEL_DIM;

        //and return a Rect with the given coordinates
        return rect::Rect::new(cx as i32, cy as i32, 
                               constants::PIXEL_DIM, constants::PIXEL_DIM);
    }

    /// Gets the state of the `Pixel`
    ///
    /// # Returns 
    ///
    /// The state of the `Pixel`
    pub fn get_state(&self) -> PixelState {
        return self.state; //return the state field
    }

    /// Toggles the state of the `Pixel`
    pub fn toggle(&mut self) {
        self.state = !self.state;        
    }

    /// Turns the `Pixel` off
    pub fn turn_off(&mut self) {
        self.state = PixelState::OFF; //set the state to off
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the Pixel code
    use super::*;

    //this test ensures that new pixels are off
    #[test]
    fn test_new_state_is_off() {
        let pix = Pixel::new(0, 0);
        assert_eq!(pix.state, PixelState::OFF);
    }

    //this test checks toggling the state of a pixel
    #[test]
    fn test_state_toggle() {
        let mut pix = Pixel::new(0, 0);
        let st = pix.state;
        pix.toggle();
        assert_ne!(st, pix.state);
        assert_eq!(st, !pix.state);
    }

    //this test checks calculation of the bounds
    //of a pixel
    #[test]
    fn test_bounds() {
        let pix1 = Pixel::new(1, 1);
        let pix2 = Pixel::new(4, 3);
        let bds1 = pix1.get_bounds();
        let bds2 = pix2.get_bounds();
        assert_eq!(bds1.x(), 1 * constants::PIXEL_DIM as i32);
        assert_eq!(bds1.y(), 1 * constants::PIXEL_DIM as i32);
        assert_eq!(bds2.x(), 4 * constants::PIXEL_DIM as i32);
        assert_eq!(bds2.y(), 3 * constants::PIXEL_DIM as i32);
        assert_eq!(bds1.width(), constants::PIXEL_DIM);
        assert_eq!(bds1.height(), constants::PIXEL_DIM);
    }

    //this test checks the turn_off method
    #[test]
    fn test_turn_off() {
        let mut pix = Pixel::new(0, 0);
        pix.state = PixelState::ON;
        pix.turn_off();
        assert_eq!(pix.state, PixelState::OFF);
    }
}

//end of file
