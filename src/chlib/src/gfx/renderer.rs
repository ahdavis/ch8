/*
 * renderer.rs
 * Defines a struct that represents a renderer
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::pixels::Color;
use super::super::util::constants;
use super::ScreenBuffer;
use super::PixelState;
use super::Sprite;

/// Renders Chip-8 graphics
pub struct Renderer<'a> {
    buffer: ScreenBuffer, //manages the screen
    was_updated: bool, //was the screen updated?
    canvas: &'a mut Canvas<Window> //the rendering target
}

//implementation
impl<'a> Renderer<'a> {
    /// Creates a new `Renderer` instance
    ///
    /// # Argument
    ///
    /// * `new_canvas` - The SDL2 canvas to render to 
    ///
    /// # Returns 
    ///
    /// A new `Renderer` instance targeting the given canvas
    pub fn new(new_canvas: &mut Canvas<Window>) -> Renderer {
        //create and return the object
        return Renderer {
            buffer: ScreenBuffer::new(),
            was_updated: false,
            canvas: new_canvas 
        };
    }

    /// Returns the state of the pixel at
    /// given coordinates on the screen
    /// 
    /// # Arguments
    ///
    /// * `x` - The x-coordinate to inspect
    /// * `y` - The y-coordinate to inspect
    ///
    /// # Returns
    ///
    /// The state of the pixel at the given coordinates
    pub fn state_at_coords(&self, x: u32, y: u32) -> PixelState {
        return self.buffer.state_at_coords(x, y); //delegate the method to the buffer
    }

    /// Toggles the state of the pixel at
    /// given coordinates on the screen
    /// 
    /// # Arguments
    ///
    /// * `x` - The x-coordinate to toggle
    /// * `y` - The y-coordinate to toggle
    ///
    /// # Returns
    ///
    /// Whether the pixel was on and then turned off
    pub fn xor_at_coords(&mut self, x: u32, y: u32) -> bool {
        let res = self.buffer.xor_at_coords(x, y); //delegate the method to the buffer
        self.was_updated = true; //set the update flag
        return res; //and return the result
    }

    /// Renders a sprite to the screen
    /// 
    /// # Arguments
    ///
    /// * `spr` - The `Sprite` to render
    /// * `x` - The x-coordinate to render the sprite at
    /// * `y` - The y-coordinate to render the sprite at
    /// 
    /// # Returns
    ///
    /// Whether any pixels were flipped from on to off
    /// while drawing the sprite
    pub fn draw_sprite(&mut self, spr: &Sprite, x: u32, y: u32) -> bool {
        //declare the return value
        let mut ret = false;

        //draw the sprite
        for xline in 0..8 {
            for yline in 0..spr.height() {
                if spr.has_pixel_at(xline, yline) {
                    if self.xor_at_coords(x + (xline as u32), y + (yline as u32)) {
                        ret = true;
                    }
                }
            }
        }

        //and return the populated return value
        return ret;
    }

    /// Clears the rendering target
    pub fn clear(&mut self) {
        self.buffer.clear(); //clear the buffer
        self.was_updated = true; //and set the update flag
    }

    /// Renders the current frame
    pub fn update(&mut self) {
        //make sure that the screen was updated
        if !self.was_updated {
            return;
        }

        //clear the canvas
        self.canvas.set_draw_color(Color::RGB(0x00, 0x00, 0x00));
        self.canvas.clear();

        //draw the pixels
        self.canvas.set_draw_color(Color::RGB(0xFF, 0xFF, 0xFF));
        for x in 0..constants::WIN_WIDTH {
            for y in 0..constants::WIN_HEIGHT {
                if self.buffer.state_at_coords(x, y) == PixelState::ON {
                    match self.buffer.bounds_at_coords(x, y) {
                        Some(b) => {
                            self.canvas.fill_rect(b).unwrap();
                        }
    
                        None => {
                            //do nothing
                        }
                    }
                }
            }
        }

        //update the target
        self.canvas.present();

        //and clear the update flag
        self.was_updated = false;
    }
}

//end of file
