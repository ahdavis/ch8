/*
 * pixel_state.rs
 * Enumerates states of display pixels
 * Created on 8/31/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no usage statements

/// The state of a `Pixel`
#[derive(Debug, PartialEq, Copy)]
pub enum PixelState {
    ON,  //pixel is on
    OFF  //pixel is off
}

//trait implementations

//Not implementation
impl std::ops::Not for PixelState {
    //define the output type
    type Output = PixelState;

    /// Inverts the state
    /// 
    /// # Returns
    ///
    /// The opposite state of `self`
    fn not(self) -> Self::Output {
        return match self {
            PixelState::OFF => PixelState::ON,
            PixelState::ON => PixelState::OFF 
        }
    }
}

//Clone implementation
impl Clone for PixelState {
    /// Clones the `PixelState`
    ///
    /// # Returns
    ///
    /// A clone of the enum instance
    fn clone(&self) -> Self {
        return *self;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the PixelState code
    use super::PixelState;

    //this test checks inversion of states
    #[test]
    fn test_invert() {
        assert_eq!(!PixelState::OFF, PixelState::ON);
        assert_eq!(!PixelState::ON, PixelState::OFF);
    }
}

//end of file
