/*
 * sprite.rs
 * Defines a struct that represents a sprite
 * Created on 9/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/// A drawable sprite
pub struct Sprite {
    bytes: Vec<u8>, //the raw bytes that make up the sprite
    height: usize   //the height of the sprite (in pixels)
}

//implementation
impl Sprite {
    /// Creates a new `Sprite` instance
    ///
    /// # Argument
    ///
    /// * `new_bytes` - The bytes that make up the sprite
    ///
    /// # Returns
    ///
    /// A new `Sprite` instance with the given properties
    pub fn new(new_bytes: &[u8]) -> Sprite {
        //get the length of the byte array
        let len = new_bytes.len();

        //convert the bytes to a vector
        let tbytes = new_bytes.to_vec();

        //and return the sprite object
        return Sprite {
            bytes: tbytes,
            height: len 
        }
    }

    /// Gets the height of the `Sprite`
    ///
    /// # Returns
    ///
    /// The height of the `Sprite` (in pixels)
    pub fn height(&self) -> usize {
        return self.height;
    }

    /// Gets whether the pixel at
    /// a given position in the `Sprite`
    /// is on 
    ///
    /// # Arguments
    ///
    /// * `x` - The x-coordinate to check
    /// * `y` - The y-coordinate to check
    ///
    /// # Returns
    ///
    /// Whether the pixel at `(x, y)` is on
    pub fn has_pixel_at(&self, x: usize, y: usize) -> bool {
        //validate the arguments
        if x >= 8 {
            return false;
        }
        if y >= self.height {
            return false;
        }

        //get the byte at the given y-coordinate
        let byte = self.bytes[y];

        //flip the x-coordinate around so it
        //indexes from the right, making the
        //math easier
        let xcomp = 8 - x;

        //and return whether the bit is set
        return (byte & (1 << (xcomp - 1))) != 0; 
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the Sprite struct
    use super::*;

    //this test checks that the height
    //of the sprite is generated properly
    #[test]
    fn test_height_gen() {
        let spr = Sprite::new(&[0xFF, 0x00, 0xFF]);
        assert_eq!(spr.height(), 3);
    }

    //this test checks that pixel checking
    //works properly
    #[test]
    fn test_pixel_check() {
        let spr = Sprite::new(&[0xFE, 0xFF, 0x00]);
        assert_eq!(spr.has_pixel_at(0, 0), true);
        assert_eq!(spr.has_pixel_at(7, 0), false);
        assert_eq!(spr.has_pixel_at(0, 1), true);
        assert_eq!(spr.has_pixel_at(1, 1), true);
        assert_eq!(spr.has_pixel_at(0, 2), false);
        assert_eq!(spr.has_pixel_at(8, 0), false);
        assert_eq!(spr.has_pixel_at(9, 0), false);
        assert_eq!(spr.has_pixel_at(3, 3), false);
    }
}

//end of file
