/*
 * sound_timer.rs
 * Defines a struct that represents a sound timer
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//crate imports
extern crate num;
extern crate sdl2;

//imports
use super::super::audio::Beeper;
use super::Timer;
use std::time::Instant;
use std::time::Duration;
use std::thread::sleep;

/// A timer that counts down
/// at one tick per second 
/// and plays a sound on
/// each tick 
pub struct SoundTimer {
    ticks: u64, //the number of ticks left until the timer elapses
    prev_time: Instant, //the last time the timer was ticked
    beeper: Beeper //used to play sound
}

//struct implementation
impl SoundTimer {
    /// Creates a new `SoundTimer` instance
    ///
    /// # Argument
    ///
    /// * `ctxt` - The audio context for the timer
    /// 
    /// # Returns 
    ///
    /// A new `SoundTimer` instance
    pub fn new(ctxt: &mut sdl2::AudioSubsystem) -> Self {
        return SoundTimer {
            ticks: 0,
            prev_time: Instant::now(),
            beeper: Beeper::new(ctxt)
        };
    }
}

//trait implementation
impl Timer for SoundTimer {
    /// Gets the current number of ticks
    /// left on the timer
    /// 
    /// # Returns
    ///
    /// The number of ticks left
    fn get_ticks(&self) -> u64 {
        return self.ticks;
    }

    /// Sets the number of ticks 
    /// left on the timer
    /// 
    /// # Argument
    ///
    /// * `new_ticks` - The new number of ticks
    fn set_ticks(&mut self, new_ticks: u64) {
        self.ticks = new_ticks;
    }

    /// Ticks the timer
    fn tick(&mut self) {
        //get the current tick
        let cur_tick = Instant::now();

        //calculate the elapsed time since the
        //last tick
        let elapsed_time = cur_tick.duration_since(self.prev_time);

        //determine how much time is left in 1/60 second
        let remaining_time = match Self::ONE_HERTZ.checked_sub(elapsed_time) {
                                    Some(d) => {
                                        d
                                    }
                                    None => {
                                        Duration::from_secs(0)
                                    }
                             };

        //tick the timer if it's nonzero
        if self.ticks > 0 {
            sleep(remaining_time);
            let elapsed_ticks = num::clamp(elapsed_time.as_millis(), 1, std::u64::MAX as u128) as u64;
            if remaining_time.as_millis() > 0 {
                self.ticks = num::clamp(self.ticks as i64 - elapsed_ticks as i64, 0, self.ticks as i64) as u64;
            } else {
                self.ticks -= 1;
            }
            if !self.beeper.is_playing() {
                self.beeper.play();
            }
        } else {
            if self.beeper.is_playing() {
                self.beeper.stop();
            }
        }

        //and update the tick time
        self.prev_time = Instant::now();
    }

}


//unit tests
#[cfg(test)]
mod tests {
    //imports
    use super::*;

    //this test checks ticking the timer normally
    #[test]
    #[ignore]
    fn test_normal_tick() {
        let sdl = sdl2::init().unwrap();
        let mut asdl = sdl.audio().unwrap();
        let mut timer = SoundTimer::new(&mut asdl);
        timer.set_ticks(3);
        assert_eq!(timer.get_ticks(), 3);
        timer.tick();
        assert_eq!(timer.get_ticks(), 2);
        timer.tick();
        assert_eq!(timer.get_ticks(), 1);
        timer.tick();
        assert_eq!(timer.get_ticks(), 0);
        timer.tick();
        assert_eq!(timer.get_ticks(), 0);
    }
}

//end of file
