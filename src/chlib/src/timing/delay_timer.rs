/*
 * delay_timer.rs
 * Defines a struct that represents a delay timer
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//crate import
extern crate num;

//imports
use super::Timer;
use std::time::Instant;
use std::time::Duration;
use std::thread::sleep;

/// A timer that counts down
/// at one tick per second
pub struct DelayTimer {
    ticks: u64, //the number of ticks left until the timer elapses
    prev_time: Instant //the last time the timer was ticked
}

//struct implementation
impl DelayTimer {
    /// Creates a new `DelayTimer` instance
    ///
    /// # Returns
    ///
    /// A new `DelayTimer` instance
    pub fn new() -> Self {
        return DelayTimer {
            ticks: 0,
            prev_time: Instant::now()
        };
    }
}

//trait implementation
impl Timer for DelayTimer {
    /// Gets the current number of ticks
    /// left on the timer
    /// 
    /// # Returns
    ///
    /// The number of ticks left
    fn get_ticks(&self) -> u64 {
        return self.ticks;
    }

    /// Sets the number of ticks 
    /// left on the timer
    /// 
    /// # Argument
    ///
    /// * `new_ticks` - The new number of ticks
    fn set_ticks(&mut self, new_ticks: u64) {
        self.ticks = new_ticks;
    }

    /// Ticks the timer
    fn tick(&mut self) {
        //get the current tick
        let cur_tick = Instant::now();

        //calculate the elapsed time since the
        //last tick
        let elapsed_time = cur_tick.duration_since(self.prev_time);

        //determine how much time is left in 1/60 second
        let remaining_time = match Self::ONE_HERTZ.checked_sub(elapsed_time) {
                                    Some(d) => {
                                        d
                                    }
                                    None => {
                                        Duration::from_secs(0)
                                    }
                             };

        //tick the timer if it's nonzero
        if self.ticks > 0 {
            sleep(remaining_time);
            let elapsed_ticks = num::clamp(elapsed_time.as_millis(), 1, std::u64::MAX as u128) as u64;
            if remaining_time.as_millis() > 0 {
                self.ticks = num::clamp(self.ticks as i64 - elapsed_ticks as i64, 0, self.ticks as i64) as u64;
            } else {
                self.ticks -= 1;
            }
        }

        //and update the tick time
        self.prev_time = Instant::now();
    }

}


//unit tests
#[cfg(test)]
mod tests {
    //imports
    use super::*;

    //this test checks ticking the timer normally
    #[test]
    fn test_normal_tick() {
        let mut timer = DelayTimer::new();
        timer.set_ticks(3);
        assert_eq!(timer.get_ticks(), 3);
        timer.tick();
        assert_eq!(timer.get_ticks(), 2);
        timer.tick();
        assert_eq!(timer.get_ticks(), 1);
        timer.tick();
        assert_eq!(timer.get_ticks(), 0);
        timer.tick();
        assert_eq!(timer.get_ticks(), 0);
    }

    //this test checks ticking the timer
    //with delays inbetween ticks
    #[test]
    fn test_delayed_tick() {
        let mut timer = DelayTimer::new();
        timer.set_ticks(10);
        assert_eq!(timer.get_ticks(), 10);
        sleep(Duration::from_millis(3));
        timer.tick();
        assert_eq!(timer.get_ticks(), 7);
        sleep(Duration::from_millis(9));
        timer.tick();
        assert_eq!(timer.get_ticks(), 0);
    }

    //this test checks ticking multiple timers
    //at once
    #[test]
    fn test_multiple_ticks() {
        let mut t1 = DelayTimer::new();
        let mut t2 = DelayTimer::new();
        t1.set_ticks(3);
        t2.set_ticks(2);
        assert_eq!(t1.get_ticks(), 3);
        assert_eq!(t2.get_ticks(), 2);
        t1.tick();
        t2.tick();
        assert_eq!(t1.get_ticks(), 2);
        assert_eq!(t2.get_ticks(), 1);
        t1.tick();
        t2.tick();
        assert_eq!(t1.get_ticks(), 1);
        assert_eq!(t2.get_ticks(), 0); 
        t1.tick();
        t2.tick();
        assert_eq!(t1.get_ticks(), 0);
        assert_eq!(t2.get_ticks(), 0);
    }
}

//end of file
