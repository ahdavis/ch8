/*
 * timer.rs
 * Defines a trait to be implemented by timers
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
use std::time::Duration;

/// Defines the interface of a timing device
pub trait Timer {
    /// A close approximation of 1/60 of a second
    const ONE_HERTZ: Duration = Duration::from_micros(16666);

    /// Returns the current tick value 
    /// of the `Timer`
    ///
    /// # Returns
    ///
    /// The current tick of the timer
    fn get_ticks(&self) -> u64;

    /// Sets the tick value of the `Timer`
    ///
    /// # Argument
    ///
    /// * `new_tick` - The new tick value of the timer
    fn set_ticks(&mut self, new_tick: u64);

    /// Ticks the `Timer`
    fn tick(&mut self);
}

//end of file
