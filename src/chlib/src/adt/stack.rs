/*
 * stack.rs
 * Defines a trait to be implemented by stacks
 * Created on 9/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/// Describes the interface of
/// a stack
pub trait Stack<T> {
    /// Creates a new `Stack` instance
    ///
    /// # Returns
    ///
    /// A new `Stack` instance
    fn new() -> Self;

    /// Pushes a value onto the `Stack`
    ///
    /// # Argument
    ///
    /// * `val` - The value to push
    /// 
    /// # Returns
    ///
    /// Whether the value was pushed successfully
    fn push(&mut self, val: T) -> bool;

    /// Pops a value off of the `Stack`
    ///
    /// # Returns
    ///
    /// The popped value, wrapped in an `Option`
    fn pop(&mut self) -> Option<T>;

    /// Returns the value on top of the `Stack`
    ///
    /// # Returns
    ///
    /// An immutable reference to the value
    /// on top of the `Stack`, wrapped in
    /// an `Option`
    fn peek(&self) -> Option<&T>;

    /// Returns the value on top of the `Stack`
    ///
    /// # Returns
    ///
    /// A mutable reference to the value
    /// on top of the `Stack`, wrapped in
    /// an `Option`
    fn peek_mut(&mut self) -> Option<&mut T>;

    /// Returns whether the `Stack` is empty
    ///
    /// # Returns
    ///
    /// Whether the `Stack` is empty
    fn is_empty(&self) -> bool;

    /// Returns the number of items in the `Stack`
    ///
    /// # Returns
    ///
    /// The number of items in the `Stack`
    fn count(&self) -> u32;

    /// Clears the `Stack`
    fn clear(&mut self);
}

//end of file
