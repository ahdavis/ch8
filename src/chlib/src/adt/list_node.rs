/*
 * list_node.rs
 * Defines a structure that represents a node in a linked list
 * Created on 8/30/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no usage statement

/// A node in a linked-list stack
pub struct ListNode<T> {
    pub value: T,
    pub next: Option<Box<ListNode<T>>>
}

//implementation
impl<T> ListNode<T> {
    /// Creates a new `ListNode` instance 
    ///
    /// # Arguments
    ///
    /// * `new_value` The value of the new node
    /// * `new_next` The next node in the list
    ///
    /// # Returns
    ///
    /// A new `ListNode` instance with the given properties
    pub fn new(new_value: T, new_next: Option<ListNode<T>>) -> ListNode<T> {
        //generate and return a new instance
        return ListNode {
            value: new_value,
            next: match new_next {
                Some(s) => {
                    Some(Box::new(s))
                }

                None => {
                    None 
                }
            }
        };
    }

    /// Extracts the next node in the list. 
    /// The instance this method is called
    /// on is consumed by it. 
    /// 
    /// # Returns
    ///
    /// If the current node has a next node, it will be returned
    /// wrapped in a `Some` value. Otherwise, `None` will be returned. 
    pub fn extract_next(self) -> Option<ListNode<T>> {
        //extract the next field
        return match self.next {
            Some(b) => {
                Some(*b)
            }
            
            None => {
                None 
            }
        }
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the StackNode code
    use super::*;

    //this test checks linking one node to another
    #[test]
    fn test_link_creation() {
        let head = ListNode::new(42, None);
        assert_eq!(head.value, 42);
        assert!(head.next.is_none());
        let head = ListNode::new(32, Some(head));
        assert_eq!(head.value, 32);
        assert!(head.next.is_some()); 
    }

    //this test checks the extract_next method
    //to make sure it returns proper values
    #[test]
    fn test_extract_next() {
        let head = ListNode::new(42, None);
        assert!(head.extract_next().is_none());
        let head = ListNode::new(32, Some(ListNode::new(42, None)));
        assert!(head.extract_next().is_some());
    }
}

//end of file
