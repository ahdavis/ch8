/*
 * list_stack.rs
 * Defines a linked-list implementation of a stack
 * Created on 8/30/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use super::ListNode;
use super::Stack;

/// A linked-list implementation of a stack
pub struct ListStack<T> {
    head: Option<ListNode<T>>, //the top of the stack
    size: u32 // the number of elements on the stack
}

//implementation
impl<T> Stack<T> for ListStack<T> {
    /// Creates a new `ListStack` instance
    ///
    /// # Returns
    ///
    /// A new `ListStack` instance
    fn new() -> Self {
        //return a ListStack with an empty head
        return ListStack {
            head: None,
            size: 0
        };
    }

    /// Pushes a value onto the `ListStack`
    ///
    /// # Arguments
    ///
    /// * `val` - The value to push 
    ///
    /// # Returns 
    ///
    /// Whether the value was pushed successfully
    fn push(&mut self, val: T) -> bool {
        //get the new node
        let new_node = ListNode::new(val, self.head.take());

        //increment the size field
        self.size += 1;

        //add it to the stack
        self.head = Some(new_node);

        //and return a success
        return true;
    }

    /// Pops a value off of the `ListStack`
    ///
    /// # Returns
    ///
    /// The value on top of the stack,
    /// wrapped in an `Option`
    fn pop(&mut self) -> Option<T> {
        //decrement the size
        self.size -= 1;

        //and pop the value off the stack
        //and return it
        return self.head.take().map(|node| {
            match node.next {
                Some(b) => {
                    self.head = Some(*b);
                }

                None => {
                    self.head = None;
                }
            }
            node.value 
        });
    }

    /// Returns the value on top of the `ListStack`
    ///
    /// # Returns
    ///
    /// An immutable reference to the value
    /// on top of the stack, wrapped in 
    /// an `Option`
    fn peek(&self) -> Option<&T> {
        return match self.head.as_ref() {
            Some(h) => {
               Some(&h.value) 
            }
            None => {
                None
            }
        };
    }

    /// Returns the value on top of the `ListStack`
    ///
    /// # Returns 
    ///
    /// A mutable reference to the value
    /// on top of the stack, wrapped in
    /// an `Option`
    fn peek_mut(&mut self) -> Option<&mut T> {
        return match self.head.as_mut() {
            Some(h) => {
                Some(&mut h.value)
            }
            None => {
                None
            }
        };
    }

    /// Returns the size of the `ListStack`
    /// 
    /// # Returns
    ///
    /// The number of elements in the stack
    fn count(&self) -> u32 {
        return self.size;
    }

    /// Returns whether the `ListStack` is empty
    ///
    /// # Returns 
    ///
    /// Whether the stack is empty
    fn is_empty(&self) -> bool {
        return self.head.is_none(); //return whether the head is None
    }

    /// Clears the `ListStack`
    fn clear(&mut self) {
        while !self.is_empty() {
            self.pop();
        }
    }
}

// Drop trait implementation
impl<T> Drop for ListStack<T> {
    /// Releases the memory held by the `ListStack`
    fn drop(&mut self) {
        self.clear();
    }
}

//Unit tests
#[cfg(test)]
mod tests {
    //import the ListStack class
    use super::*;

    //this test checks that items can be
    //pushed onto the stack properly
    #[test]
    fn test_push() {
            let mut s: ListStack<i32> = ListStack::new();
            assert_eq!(s.size, 0);
            s.push(42);
            assert_eq!(s.size, 1); 
    }

    //this test checks that items can be
    //popped off the stack properly
    #[test]
    fn test_pop() {
        let mut s: ListStack<i32> = ListStack::new();
        s.push(42);
        assert_eq!(s.size, 1);
        let p = s.pop();
        assert_eq!(s.size, 0);
        assert!(p.is_some());
        assert_eq!(p.unwrap(), 42);
    }

    //this test checks that items can be
    //looked at while on the stack
    #[test]
    fn test_peek() {
        let mut s: ListStack<i32> = ListStack::new();
        let p = s.peek();
        assert!(p.is_none());
        s.push(42);
        let p = s.peek();
        assert!(p.is_some());
        assert_eq!(p.unwrap(), &42);
    }

    //this test checks that the size
    //of the stack can be inspected
    #[test]
    fn test_count() {
        let mut s: ListStack<i32> = ListStack::new();
        assert_eq!(s.size, s.count());
        s.push(32);
        assert_eq!(s.size, s.count());
        s.pop();
        assert_eq!(s.size, s.count());
    }

    //this test checks that whether
    //the stack is empty can be checked
    #[test]
    fn test_is_empty() {
        let mut s: ListStack<i32> = ListStack::new();
        assert_eq!(s.is_empty(), true);
        s.push(42);
        assert_eq!(s.is_empty(), false);
        s.pop();
        assert_eq!(s.is_empty(), true);
    }

    //this test checks that the stack 
    //can be cleared
    #[test]
    fn test_clear() {
        let mut s: ListStack<i32> = ListStack::new();
        assert_eq!(s.is_empty(), true);
        s.push(42);
        s.push(34);
        s.push(35);
        assert_eq!(s.is_empty(), false);
        s.clear();
        assert_eq!(s.is_empty(), true);
    }
}

//end of file
