/*
 * memory.rs
 * Defines a struct that represents Chip-8 RAM
 * Created on 9/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
use super::super::util::Program;
use super::super::gfx::Sprite;

/// The main memory for the emulator
pub struct Memory {
   data: [u8; Memory::MEM_SIZE] // The bytes that make up the memory
}

//implementation
impl Memory {
    //constants
    
    /// The number of bytes available to access
    pub const MEM_SIZE: usize = 0x1000;

    /// The address of the start of font memory
    pub const FNT_START: u16 = 0x0050;

    /// The address of the end of font memory
    pub const FNT_END: u16 = 0x00A0;

    /// The address of the start of program memory
    pub const PROG_START: u16 = 0x0200;

    //methods
    
    /// Creates a new `Memory` instance
    ///
    /// # Returns
    ///
    /// A new `Memory` instance
    pub fn new() -> Memory {
        //create the instance
        let mut ret = Memory {
            data: [0; Memory::MEM_SIZE]
        };

        //init font memory
        ret.init_font();

        //and return the initialized memory
        return ret;
    }

    /// Gets the byte at a given address
    ///
    /// # Argument
    ///
    /// * `addr` - The address to inspect
    ///
    /// # Returns
    ///
    /// The byte at the given address
    pub fn get_byte(&self, addr: u16) -> u8 {
        return self.data[(addr & 0x0FFF) as usize]; //return the byte
    }

    /// Sets the byte at a given address
    ///
    /// # Arguments
    ///
    /// * `val` - The byte to set
    /// * `addr` - The address to modify
    pub fn set_byte(&mut self, val: u8, addr: u16) {
        self.data[(addr & 0x0FFF) as usize] = val;
    }

    /// Gets the word at a given address
    ///
    /// # Argument
    ///
    /// * `addr` - The address to read from
    ///
    /// # Returns
    ///
    /// The two-byte word at the given address
    pub fn get_word(&self, addr: u16) -> u16 {
        //get the bytes that make up the word
        let lsb = self.get_byte(addr) as u16;
        let msb = self.get_byte(addr + 1) as u16;

        //merge them
        let ret: u16 = (msb << 8) | lsb;

        //and return the result
        return ret;
    }

    /// Sets the word at a given address
    ///
    /// # Arguments
    ///
    /// * `val` - The word to set
    /// * `addr` - The address to modify
    pub fn set_word(&mut self, val: u16, addr: u16) {
        //separate the word into its components
        let lsb = (val & 0xFF) as u8;
        let msb = (val >> 8) as u8;

        //and store the bytes in memory
        self.set_byte(lsb, addr);
        self.set_byte(msb, addr + 1);
    }

    /// Loads a `Program` into memory
    ///
    /// # Arguments
    ///
    /// * `prog` - The program to load
    ///
    /// # Returns
    ///
    /// Whether the program was loaded successfully
    pub fn load_program(&mut self, prog: &Program) -> bool {
        //make sure the program is not too long
        if (prog.len() + Memory::PROG_START as usize) >= Memory::MEM_SIZE {
            return false;
        }

        //load the program
        let bytes = prog.get_bytes();
        for i in 0..prog.len() {
            self.set_byte(bytes[i], i as u16 + Memory::PROG_START);
        }

        //and return a success
        return true;
    }

    /// Gets a sprite from memory
    /// 
    /// # Arguments
    ///
    /// * `addr` - The address of the first byte of the sprite
    /// * `height` - The height of the sprite (in pixels)
    ///
    /// # Returns
    ///
    /// A new `Sprite` instance described by the arguments
    pub fn get_sprite(&self, addr: u16, height: u16) -> Sprite {
        //create a vector to hold the sprite data
        let mut data: Vec<u8> = Vec::new();

        //loop and get the sprite data
        for i in 0..height {
            data.push(self.get_byte(i + addr));
        }

        //and return the sprite
        return Sprite::new(&data[..]);
    }

    /// Initializes font memory
    fn init_font(&mut self) {
        //create an array of the font symbols
	let font: [u8; 80] = [
	        0xF0, 0x90, 0x90, 0x90, 0xF0, //0
        	0x20, 0x60, 0x20, 0x20, 0x70, //1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
		0x90, 0x90, 0xF0, 0x10, 0x10, //4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
		0xF0, 0x10, 0x20, 0x40, 0x40, //7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
		0xF0, 0x90, 0xF0, 0x90, 0x90, //A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
		0xF0, 0x80, 0x80, 0x80, 0xF0, //C
		0xE0, 0x90, 0x90, 0x90, 0xE0, //D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
		0xF0, 0x80, 0xF0, 0x80, 0x80  //F
	];

        //and load them into memory
        for i in 0..80 {
            self.set_byte(font[i], i as u16 + Memory::FNT_START);
        }

    }
}

//unit tests
#[cfg(test)]
mod tests {
    //bring Memory into scope
    use super::*;

    //this test checks setting and getting bytes
    #[test]
    fn test_setget_byte() {
        let mut mem = Memory::new();
        mem.set_byte(0x20, 0x0000);
        assert_eq!(mem.get_byte(0x0000), 0x20);
    }

    //this test checks setting and getting words
    #[test]
    fn test_setget_word() {
        let mut mem = Memory::new();
        mem.set_word(0xFC04, 0x0000);
        assert_eq!(mem.get_word(0x0000), 0xFC04);
    }
}

//end of file
