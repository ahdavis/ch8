/*
 * regname.rs
 * Enumerates names of CPU registers
 * Created on 9/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/// The name of a `Register`
#[derive(Debug, PartialEq, Copy, Eq, Hash)]
pub enum RegName {
    V0 = 0x0,
    V1 = 0x1,
    V2 = 0x2,
    V3 = 0x3,
    V4 = 0x4,
    V5 = 0x5,
    V6 = 0x6,
    V7 = 0x7,
    V8 = 0x8,
    V9 = 0x9,
    VA = 0xA,
    VB = 0xB,
    VC = 0xC,
    VD = 0xD,
    VE = 0xE,
    VF = 0xF 
}

//implementation
impl RegName {
    /// Creates a `RegName` instance
    /// from an integer
    ///
    /// # Argument
    ///
    /// * `ri` - The index of the desired name
    /// 
    /// # Returns
    ///
    /// The `RegName` corresponding to the given index
    ///
    /// # Panics
    ///
    /// This method will panic if a value greater
    /// than 15 (`0xF`) is supplied
    pub fn from_u32(ri: u32) -> Self {
        return match ri {
            0x0 => RegName::V0,
            0x1 => RegName::V1,
            0x2 => RegName::V2,
            0x3 => RegName::V3,
            0x4 => RegName::V4,
            0x5 => RegName::V5,
            0x6 => RegName::V6,
            0x7 => RegName::V7,
            0x8 => RegName::V8,
            0x9 => RegName::V9,
            0xA => RegName::VA,
            0xB => RegName::VB,
            0xC => RegName::VC,
            0xD => RegName::VD,
            0xE => RegName::VE,
            0xF => RegName::VF,
            _ => panic!("Invalid register index: {}", ri)
        };
    }
}

//Clone implementation
impl Clone for RegName {
    /// Clones the `RegName` instance
    ///
    /// # Returns
    ///
    /// A clone of `self`
    fn clone(&self) -> Self {
        return *self;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the RegName enum
    use super::*;

    //this test checks generation of a 
    //RegName instance from an integer
    #[test]
    fn test_regname_gen() {
        let regs = [
            RegName::V0,
            RegName::V1,
            RegName::V2,
            RegName::V3,
            RegName::V4,
            RegName::V5,
            RegName::V6,
            RegName::V7,
            RegName::V8,
            RegName::V9,
            RegName::VA,
            RegName::VB,
            RegName::VC,
            RegName::VD,
            RegName::VE,
            RegName::VF 
        ];
        for i in 0..16 {
            let name = RegName::from_u32(i);
            assert_eq!(name, regs[i as usize]);
        }
    }

    //this test ensures that invalid indices
    //cannot be used to generate RegName instances
    #[test]
    #[should_panic]
    fn test_invalid_regname_gen() {
        let _unused = RegName::from_u32(16);
    }
}

//end of file
