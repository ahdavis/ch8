/*
 * register.rs
 * Defines a struct that represents a processor register
 * Created on 9/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//crate import
extern crate num;

//imports
use super::RegName;
use std::ops;
use std::cmp;

/// A processor register 
#[derive(Debug)]
pub struct Register {
    value: u8,    //the value of the register
    name: RegName //the name of the register
}

// Struct implementation
impl Register {
    /// Creates a new `Register` instance
    ///
    /// # Argument
    ///
    /// * `new_name` - The name of the register
    /// 
    /// # Returns
    ///
    /// A new `Register` instance with the
    /// given name and a value of zero
    pub fn new(new_name: RegName) -> Register {
        return Register {
            value: 0,
            name: new_name 
        };
    }

    /// Gets the name of the `Register`
    ///
    /// # Returns
    ///
    /// The name of the `Register`
    pub fn get_name(&self) -> RegName {
        return self.name;
    }

    /// Gets the value of the `Register`
    ///
    /// # Returns
    ///
    /// The value of the `Register`
    pub fn get_value(&self) -> u8 {
        return self.value;
    }

    /// Sets the value of the `Register`
    ///
    /// # Argument
    ///
    /// * `new_value` - The new value of the register
    pub fn set_value(&mut self, new_value: u8) {
        self.value = new_value;
    }
}

// Add implementation for u8
impl ops::Add<u8> for Register {
    type Output = Self;

    /// Adds a value to the `Register`
    /// 
    /// # Argument
    ///
    /// * `val` - The value to add
    /// 
    /// # Returns
    ///
    /// A new `Register` with the value of 
    /// `self` added to `val`.
    fn add(self, val: u8) -> Self::Output {
        return Register {
            value: num::clamp(self.value as i16 + val as i16, 0, 0xFF) as u8,
            name: self.name
        };
    }
}

// AddAssign implementation for u8
impl ops::AddAssign<u8> for Register {
    /// Adds a value to the `Register`
    /// in-place
    ///
    /// # Argument
    ///
    /// * `val` - The value to add
    fn add_assign(&mut self, val: u8) {
        self.value = num::clamp(self.value as i16 + val as i16, 0, 0xFF) as u8;
    }
}

// Add implementation for registers
impl ops::Add<&Register> for Register {
    type Output = Self;

    /// Adds one `Register` to `self`
    ///
    /// # Argument
    ///
    /// * `reg` - The register to add to `self`
    ///
    /// # Returns
    ///
    /// A new `Register` instance with the value
    /// of `self` added to `reg`
    fn add(self, reg: &Register) -> Self::Output {
        return Register {
            value: num::clamp(self.value as i16 + reg.value as i16, 0, 0xFF) as u8,
            name: self.name 
        };
    }
}

// AddAssign implementation for registers
impl ops::AddAssign<&Register> for Register {
    /// Adds one `Register` to `self` in-place
    ///
    /// # Argument
    ///
    /// * `reg` - The register to add to `self`
    fn add_assign(&mut self, reg: &Register) {
        self.value = num::clamp(self.value as i16 + reg.value as i16, 0, 0xFF) as u8;
    }
}

// Sub implementation 
impl ops::Sub<&Register> for Register {
    type Output = Self;

    /// Subtracts one `Register` from `self`
    ///
    /// # Argument
    ///
    /// * `reg` - The register to subtract
    /// 
    /// # Returns 
    ///
    /// A new `Register` with the value of `reg` 
    /// subtracted from `self`
    fn sub(self, reg: &Register) -> Self::Output {
        return Register {
            value: num::clamp(self.value as i16 - reg.value as i16, 0, 0xFF) as u8,
            name: self.name 
        };
    }
}

// SubAssign implementation
impl ops::SubAssign<&Register> for Register {
    /// Subtracts one `Register` from `self` in-place
    ///
    /// # Argument
    ///
    /// * `reg` - The register to subtract from `self`
    fn sub_assign(&mut self, reg: &Register) {
        self.value = num::clamp(self.value as i16 - reg.value as i16, 0, 0xFF) as u8;
    }
}

//PartialEq implementation
impl cmp::PartialEq for Register {
    /// Compares another `Register` to `self`
    ///
    /// # Argument
    ///
    /// * `reg` - The register to compare `self` to
    ///
    /// # Returns
    ///
    /// Whether `self` and `reg` are equivalent
    fn eq(&self, reg: &Register) -> bool {
        return self.value == reg.value;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the Register struct
    use super::*;

    //this test checks that the default
    //value of a register is 0
    #[test]
    fn test_default_value_is_0() {
        let reg = Register::new(RegName::V0);
        assert_eq!(reg.value, 0x00);
    }

    //this test checks addition operations
    //between a register and a byte
    #[test]
    fn test_add_byte() {
        let mut reg = Register::new(RegName::V0);
        reg += 0x44;
        assert_eq!(reg.value, 0x44);
    }

    //this test checks addition operations
    //between two registers
    #[test]
    fn test_add_register() {
        let mut reg1 = Register::new(RegName::V0);
        let mut reg2 = Register::new(RegName::V1);
        reg2.value = 0x22;
        reg1 += &reg2;
        assert_eq!(reg1.value, 0x22);
    }

    //this test checks subtraction operations
    //between two registers
    #[test]
    fn test_sub_register() {
        let mut reg1 = Register::new(RegName::V0);
        reg1.value = 0x45;
        let mut reg2 = Register::new(RegName::V1);
        reg2.value = 0x23; 
        reg1 -= &reg2;
        assert_eq!(reg1.value, 0x22);
    }

    //this test checks equality between two registers
    #[test]
    fn test_reg_equals() {
        let mut reg1 = Register::new(RegName::V0);
        let reg2 = Register::new(RegName::V1);
        assert_eq!(reg1, reg2);
        reg1.value = 0x44;
        assert_ne!(reg1, reg2);
    }

    //this test checks out-of-range addition
    #[test]
    fn test_oob_addition() {
        let mut reg1 = Register::new(RegName::V0);
        reg1.value = 0x33;
        reg1 += 0xFF;
        assert_eq!(reg1.value, 0xFF);
        let mut reg2 = Register::new(RegName::V1);
        reg1.value = 0x33;
        reg2.value = 0xFF;
        reg1 += &reg2;
        assert_eq!(reg1.value, 0xFF);
    }

    //this test checks out-of-range subtraction
    #[test]
    fn test_oob_subtraction() {
        let mut reg1 = Register::new(RegName::V0);
        let mut reg2 = Register::new(RegName::V1);
        reg1.value = 0x10;
        reg2.value = 0x22;
        reg1 -= &reg2;
        assert_eq!(reg1.value, 0x00);
    }
}

//end of file
