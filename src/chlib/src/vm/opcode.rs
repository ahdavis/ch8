/*
 * opcode.rs
 * Defines a struct that represents an opcode
 * Created on 9/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/// A Chip-8 opcode
#[derive(Debug, PartialEq, Copy)]
pub struct Opcode {
    data: u16, //the actual underlying data
}

//implementation
impl Opcode {
    /// Creates a new `Opcode` instance
    ///
    /// # Arguments
    ///
    /// * `code` - The numeric opcode
    /// 
    /// # Returns
    ///
    /// A new `Opcode` instance with the given properties
    pub fn new(code: u16) -> Self {
        return Opcode {
            data: code 
        };
    }

    /// Returns the `n`th nibble of the `Opcode`
    /// indexed from the right 
    ///
    /// # Argument
    ///
    /// * `n` - The index of the nibble to get
    ///
    /// # Returns
    ///
    /// The selected nibble of the `Opcode`
    ///
    /// # Panics
    ///
    /// This method will panic if `n` is >= 4.
    pub fn nth_nibble(&self, n: u16) -> u16 {
        //validate n
        if n >= 4 {
            panic!("Invalid nibble index: {}", n);
        }

        //calculate the bitmask
        let mask = 0xF << (n * 4);

        //get the nibble
        let nib = self.data & mask;

        //and return it shifted to the right
        return nib >> (n * 4);
    }
}

//Clone implementation
impl Clone for Opcode {
    /// Clones the `Opcode`
    ///
    /// # Returns
    ///
    /// A clone of `self`
    fn clone(&self) -> Self {
        return *self;
    }
}

//unit tests
#[cfg(test)]
mod tests {
    //import the Opcode struct
    use super::*;

    //this test checks getting the nth nibble
    #[test]
    fn test_nth_nibble() {
        let op = Opcode::new(0xFC0A);
        assert_eq!(op.nth_nibble(0), 0xA);
        assert_eq!(op.nth_nibble(1), 0x0);
        assert_eq!(op.nth_nibble(2), 0xC);
        assert_eq!(op.nth_nibble(3), 0xF);
    }

    //this test ensures that out-of-range
    //nibble indices panic
    #[test]
    #[should_panic]
    fn test_oob_nibble() {
        let op = Opcode::new(0xFC0A);
        let _unused = op.nth_nibble(4);
    }
}

//end of file
