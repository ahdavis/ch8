/*
 * main.rs
 * Entry point for ch8
 * Created on 8/30/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import sdl2
extern crate sdl2;

//import chlib
extern crate chlib;

//bring sdl modules into scope
use sdl2::pixels::Color;
use sdl2::event::Event;

/// Entry point for the emulator
fn main() -> Result<(), std::io::Error>{
    //initialize SDL
    let ctxt = sdl2::init().unwrap();
    let vid_sys = ctxt.video().unwrap();

    //create the window
    let width = chlib::util::constants::PIXEL_DIM * chlib::util::constants::WIN_WIDTH;
    let height = chlib::util::constants::PIXEL_DIM * chlib::util::constants::WIN_HEIGHT;
    let win: sdl2::video::Window = vid_sys.window("ch8", width, height)
        .position_centered().build().unwrap();

    //get the drawing canvas
    let mut canvas = win.into_canvas().build().unwrap();

    //clear the screen
    canvas.set_draw_color(Color::RGB(0x00, 0x00, 0x00));
    canvas.clear();
    canvas.present();

    //create the renderer
    let mut renderer = chlib::gfx::Renderer::new(&mut canvas);

    //get an event pumper
    let mut event_pump = ctxt.event_pump().unwrap();

    //start the game loop
    'running: loop {
        //handle events
        for event in event_pump.poll_iter() {
            match event {
                //quit event
                Event::Quit {..} => {
                    break 'running 
                }

                //any other event
                _ => {
                    //do nothing
                }
            }
        }
    
        //and render the game
        renderer.update();
    }

    //and return with no errors
    return Ok(());
}

//end of file
